module NatBool where

open import Relation.Binary.PropositionalEquality
open import Data.Product
open import Data.Nat
open import Data.Nat.Properties
open import Data.Nat.Properties.Simple

max : ℕ → ℕ → ℕ
max zero zero = zero
max zero (suc m) = suc m
max (suc n) zero = suc n
max (suc n) (suc m) = suc (max n m)

max≤ : {a b : ℕ} → (a ≤ max a b) × (b ≤ max a b)
max≤ {zero} {zero} = z≤n , z≤n
max≤ {zero} {suc b} = z≤n , ≤-refl
max≤ {suc a} {zero} = ≤-refl , z≤n
max≤ {suc a} {suc b} = s≤s (proj₁ m) , s≤s (proj₂ m) where m = max≤ {a} {b}

max≤' : (a b c : ℕ) → (a ≤ max a (max b c)) × (b ≤ max a (max b c)) × (c ≤ max a (max b c))
max≤' zero zero zero = z≤n , z≤n , z≤n
max≤' zero zero (suc c) = z≤n , (z≤n , (s≤s ≤-refl))
max≤' zero (suc b) zero = z≤n , ((s≤s ≤-refl) , z≤n)
max≤' zero (suc b) (suc c) = z≤n , ((s≤s (proj₁ h)) , s≤s ((proj₂ h))) where h = max≤ {b} {c}
max≤' (suc a) zero zero = (s≤s ≤-refl) , (z≤n , z≤n)
max≤' (suc a) zero (suc c) = (s≤s (proj₁ h)) , z≤n , (s≤s (proj₂ h)) where h = max≤ {a} {c}
max≤' (suc a) (suc b) zero = (s≤s (proj₁ h)) , (s≤s (proj₂ h)) , z≤n where h = max≤ {a} {b}
max≤' (suc a) (suc b) (suc c) = (s≤s (proj₁ h)) , ((s≤s (proj₁ (proj₂ h))) , (s≤s (proj₂ (proj₂ h)))) where h = max≤' a b c

{-
_^_ : ℕ → ℕ → ℕ
n ^ zero = 1
n ^ suc m = n * n ^ m
-}

≤add : {a b : ℕ} →  a ≤ a + b
≤add {zero} {b} = z≤n
≤add {suc a} {b} = s≤s ≤add

≤mon+ : {a b c : ℕ} → a ≤ b → a ≤ b + c
≤mon+ {a} {b} {c} q = ≤-trans {a} {b} {b + c} q ≤add

≤mon^l1 : {n : ℕ} → 1 ≤ 3 ^ n
≤mon^l1 {zero} = s≤s z≤n
≤mon^l1 {suc n} = ≤mon+ (≤mon^l1 {n})

+≤ : {a b a' b' : ℕ} → a ≤ b → a' ≤ b' → a + a' ≤ b + b'
+≤ z≤n z≤n = z≤n
+≤ (z≤n {a}) (s≤s {b}{c} q) = subst (suc b ≤_) (+-comm (suc c) a) (s≤s (≤-trans {b} {c} {c + a} q ≤add))
+≤ (s≤s p) z≤n = s≤s (+≤ p z≤n)
+≤ (s≤s p) (s≤s q) = s≤s (+≤ p (s≤s q))

≤mon^ : {a b : ℕ} → a ≤ b → 3 ^ a ≤ 3 ^ b
≤mon^ {zero} {zero} = s≤s
≤mon^ {zero} {suc b} = λ _ → ≤mon+ (≤mon^l1 {b})
≤mon^ {suc a} {zero} = λ ()
≤mon^ {suc a} {suc b} q = +≤ r (+≤ r (+≤ r z≤n)) where r = ≤mon^ {a} {b} (pred-mono q)

-- 2.1

data Tm         : Set where
  num           : ℕ → Tm
  _+Tm_         : (t t' : Tm) → Tm
  isZero        : (t : Tm) → Tm
  true          : Tm
  false         : Tm
  if_then_else_ : (t t' t'' : Tm) → Tm

height : Tm → ℕ
height (num n) = 0
height (t +Tm t') = 1 + max (height t) (height t')
height (isZero t) = 1 + height t
height true = 0
height false = 0
height (if t then t' else t'') = 1 + max (height t) (max (height t') (height t''))

trues : Tm → ℕ
trues (num n) = 0
trues (t +Tm t') = trues t + trues t'
trues (isZero t) = trues t
trues true = 1
trues false = 0
trues (if t then t' else t'') = trues t + trues t' + trues t''

lemma : (t : Tm) → trues t ≤ 3 ^ height t
lemma (num n) = z≤n
lemma (t +Tm t') =
  ≤-trans {trues t + trues t'}
                (+≤ (lemma t) (lemma t'))
                (subst ((3 ^ height t) + (3 ^ height t') ≤_) (+-assoc x x (x + 0)) almost)
  where
    x = 3 ^ max (height t) (height t')
    h = max≤ {height t} {height t'}
    almost = ≤mon+ (+≤ (≤mon^ (proj₁ h)) (≤mon^ (proj₂ h)))
lemma (isZero t) = ≤mon+ (lemma t)
lemma true = s≤s z≤n
lemma false = z≤n
lemma (if t then t' else t'') =
  ≤-trans {trues t + trues t' + trues t''}
                (+≤ (+≤ (lemma t) (lemma t')) (lemma t''))
                (subst ((3 ^ height t) + (3 ^ height t') + (3 ^ height t'') ≤_) ≡lemma almost)
  where
    x = 3 ^ max (height t) (max (height t') (height t''))
    h = max≤' (height t) (height t') (height t'')
    h₁ = proj₁ h
    h₂ = proj₁ (proj₂ h)
    h₃ = proj₂ (proj₂ h)
    almost : (3 ^ height t  + 3 ^ height t' + 3 ^ height t'' ≤ x + x + x)
    almost = +≤ (+≤ (≤mon^ h₁) (≤mon^ h₂)) (≤mon^ h₃)
    ≡lemma : x + x + x ≡ x + (x + (x + 0))
    ≡lemma = subst (λ y → x + x + y ≡ x + (x + (x + 0))) (+-comm x 0) (+-assoc x x (x + 0))

       
